from django.urls import path, re_path
from . import views

urlpatterns = [
    path('', views.ReportsView.as_view(), name='home'),
    path('about/', views.about, name='about'),
    path('reports/<int:report_id>/', views.DepartmentsView.as_view(), name='reports'),
]