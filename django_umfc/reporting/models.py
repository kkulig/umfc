from django.db import models
from django.utils import timezone

class Report(models.Model):
    date = models.DateField()

class Printer(models.Model):
    black_price = models.DecimalField(max_digits=6, decimal_places=2)
    color_price = models.DecimalField(max_digits=6, decimal_places=2)
    lease_price = models.DecimalField(max_digits=6, decimal_places=2)
    department = models.IntegerField(default = 0)
    report_id = models.ForeignKey(Report, on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now)
    install_date = models.DateTimeField(default=timezone.now)