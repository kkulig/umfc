from django.shortcuts import render
from .models import Printer, Report
from django.db.models import Sum, F
from django.views import View
from django.views.generic import (
    ListView,
    DetailView,
)

def home(request):
    return render(request, 'home.html')

class ReportsView(ListView):
	template_name = 'home.html'
	queryset = Report.objects.all()
	context_object_name = 'reports'


def about(request):
    return render(request, 'about.html', {'title': 'About'})

class DepartmentsView(View):
	def get(self, request, *args, **kwargs):
		context = {
            'depertments': Printer.objects.filter(report_id=kwargs['report_id']).values('department'),
            'sums_for_each_depertment': Printer.objects.filter(report_id=kwargs['report_id']).values('department').annotate(total=Sum(F('black_price')+F('color_price')+F('lease_price'))),
            'sum_all': Printer.objects.filter(report_id=kwargs['report_id']).aggregate(total=Sum(F('black_price')+F('color_price')+F('lease_price')))['total']
        }
		return render(request, "report-detail.html", context=context)