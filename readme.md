# Project for recrutation.
/django_umfc is projet django. Project show month raports about printers costs. Idea is to check is report perfect.
In raport detail result is department id, price and full price for all departments.

/basic_raport_km is presenting fun with create random report. Library Pandas can help to change csv to important for project values.

## Example

### reports
img/reports.PNG
### report detail 
img/report1.png

### table for reports
img/reporting_report.png
### table for printers
img/reporting_printer.png

## Stack
* Python 3.5.6
* Django 2.1.3
* Bootstrap 4
* Pandas
* For manual test used Sqlite